from Model.GameModel import GameModel
from View.GameView import GameView


class GameController():
    def __init__(self):
        self.model = GameModel()
        self.view = GameView(self)
