from kivy.core.window import Window
from kivy.app import App
from Controller.GameController import GameController


class Game(App):
    def build(self):
        self.title = "Game | Kivy"
        return GameController().view


if __name__ == "__main__":
    Window.size = (1920, 1080)
    Game().run()
