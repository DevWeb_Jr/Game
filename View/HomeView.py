from kivy.uix.image import Image
from kivy.uix.boxlayout import BoxLayout
from kivy.clock import Clock
from kivy.uix.screenmanager import Screen
from kivy.uix.button import Button


class HomeView(Screen):
    def __init__(self, **kwargs):
        BoxLayout.__init__(self, **kwargs)
        self.wallpaper = Image(source="./Assets/Images/logo.png", allow_stretch=True)
        self.add_widget(self.wallpaper)
        self.switch_button = Button(text="Map")
        self.add_widget(self.switch_button)

    def switch(self, widget):
        self.manager.current = "Map"
        self.manager.transition.direction = 'left'

    def show_wallpaper(self):
        Clock.schedule_once(self.photo)

