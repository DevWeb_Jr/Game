from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button


class GameView(BoxLayout):
    def __init__(self, controller, **kwargs):
        BoxLayout.__init__(self, **kwargs)
        self.controller = controller
        self.orientation = "vertical"
