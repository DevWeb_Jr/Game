Faire une application qui permet de créer des personnages, et de les déplacer à l'écran.

Pour créer un personnage, c'est le même menu que vous avez déjà fait, vous pouvez le reprendre à peu près tel quel.
Une fois connecté avec un personnage,
on arrive sur un menu Map où on retrouve le personnage par défaut au centre de l'écran,
et en bas à droite de l'écran 4 boutons 'haut' 'bas' 'gauche' 'droite'.

Si le personnage arrive en bas à droite de l'écran, il ne doit pas s'afficher par-dessus les boutons, mais en dessous.
Le monde simulé est un tore à 2 dimensions -> si le personnage arrive sur le bord droit, il réapparait à gauche,
si il arrive en haut il réapparait en bas, et inversement.

Si on clique sur le personnage, cela le repositionne au centre de l'écran.

Un bouton Menu en bas à gauche de l'écran permet de revenir au menu principal,
et cela sauvegarde en même temps la position du personnage dans un fichier.

Lorsque l'utilisateur se reconnectera avec ce même personnage, il sera positionner à sa dernière position enregistrée.

Il faut faire un model Personnage,
donc une classe qui contient les attributs nécessaires pour enregistrer l'état du personnage (nom, position...).
C'est cette class model Personnage() qu'il faut enregistrer dans un fichier json (Pickle).